# Task 0

Clone this repository (well done!)

# Task 1

Take a look a the two repositories:
  
  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A?
  
  faleyknight
  
  * Who made the first commit to repository A?
  
  Scott Chacon
  
  * Who made the last and first commits to repository B?
  
  The android Open Source Project made first commit.
  Shawn O. Pearce made last commit.
  
  * Who has been the most active recent contributor on repository A?  How about repository B?
  
  Joshua Nichols has been the most active recent contributor for A.
  Shawn O. Peace has been the most active recent contributor for B.
  
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
  
  Neither project is active. the last contributions were in 2012.
  Project A looks like it was finished, the last few updates were fixing the README, while Project B was moved to gerrit-review.googlesource.com.
  
  * 🤔 Which file in each project has had the most activity?

# Task 2

Setup a new eclipse project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~ 

🤔 Now setup a new bitbucket repository and have this project pushed to that repository.  You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore` file.  The one we have in this repository is a very good start.

